FROM openjdk:8-jdk-stretch AS base
WORKDIR /opt/
RUN \
    apt-get update && \
    apt-get install -y \
    build-essential \
    locales

# Install locale
RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    printf 'en_US.UTF-8 UTF-8\npt_BR.UTF-8 UTF-8\n' >> /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL pt_BR.UTF-8

# Build Python
FROM base AS python
ARG PYTHON_VERSION=3.5.7

RUN \
    apt-get install -y \
    pkg-config \
    libssl-dev \
    autoconf \
    automake \
    python-dev \
    libtool \
    psmisc \
    zlib1g-dev \
    libffi-dev \
    libbz2-dev \
    libncurses5-dev \
    liblzma-dev \
    libreadline-dev \
    tk \
    tk-dev
RUN wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz && \
    tar xvf Python-$PYTHON_VERSION.tgz && \
    mv Python-$PYTHON_VERSION Python && \
    cd Python && \
    ./configure && \
    make -j8 

# Build Final Image
FROM base
ARG SBT_VERSION=1.3.2
ARG DATABRICKS_VERSION=5.5.*
ARG MFA_DEVICE

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install Scala Build Tool
RUN \
    curl -L -o sbt.deb http://dl.bintray.com/sbt/debian/sbt-$SBT_VERSION.deb && \
    dpkg -i sbt.deb && \
    rm sbt.deb

# Install Python
COPY --from=python /opt/Python Python
RUN \
    cd Python && \
    make install && \
    cd .. && rm -rf Python

# Install AWS CLI, Databricks Connect and ifood_databricks
COPY ifood-data-platform/tools-pyspark tools-pyspark
RUN pip3 install awscli && \
    python3 -m venv env-databricks && \
    . env-databricks/bin/activate && \
    pip3 install wheel databricks-connect==$DATABRICKS_VERSION && \
    cd tools-pyspark && \
    pip3 install -r requirements-dbconnect.txt && \
    python3 setup.py bdist_wheel && \
    pip3 install . && \
    cd .. && rm -rf tools-pyspark

ENV PYSPARK_PYTHON python3

# Set up entrypoint
WORKDIR /opt/pwd
ENV MFA_DEVICE $MFA_DEVICE
COPY entrypoint.sh /usr/local/bin
ENTRYPOINT ["entrypoint.sh"]
CMD ["bash"]
