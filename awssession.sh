#!/bin/bash
USERNAME=($(aws iam get-user | grep UserName | sed 's/.*"\(.*\)",/\1/g'))
MFA_DEVICE="arn:aws:iam::037654772595:mfa/${USERNAME}"
MY_PROFILE="ifood-users"
PARAMS="$*"
read -p "Enter the token code: " TTOKEN
RESPONSE=($(aws ${PARAMS} sts get-session-token --serial-number "${MFA_DEVICE}" --token-code ${TTOKEN} --duration-seconds 86400 --output text))
if [ ${?} -ne 0 ]
then
  echo "Error getting the session token. Exiting."
  exit 1
fi
FILE_BEGIN_TOKEN="# BEGIN ${MY_PROFILE} temporary credentials"
FILE_END_TOKEN="# END ${MY_PROFILE} temporary credentials"
# remove already existing credentials if needed
sed -i -r "/${FILE_BEGIN_TOKEN}/,/${FILE_END_TOKEN}/d" ${HOME}/.aws/config
# write new credentials to file
cat <<EOF >> ${HOME}/.aws/config
${FILE_BEGIN_TOKEN}
[profile ${MY_PROFILE}]
aws_access_key_id=${RESPONSE[1]}
aws_secret_access_key=${RESPONSE[3]}
aws_session_token=${RESPONSE[4]}
region=us-east-1
[profile data-access]
region = us-east-1
role_arn = arn:aws:iam::563718358426:role/Data_Access
source_profile = ${MY_PROFILE}
${FILE_END_TOKEN}
EOF
UNTIL=${RESPONSE[2]}
echo "Access granted until ${UNTIL}"
echo "* Saved to file ${HOME}/.aws/config as ${MY_PROFILE} profile."
