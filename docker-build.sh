#!/bin/bash
set -uex

MFA_DEVICE="$1"

# Clone ifood-data-platform
if [ -d "ifood-data-platform" ]
then
    cd ifood-data-platform
    git pull
    cd ..
else
    git clone git@bitbucket.org:ifood/ifood-data-platform.git
fi

# Remove pyspark dependency, as it would conflict with databricks-connect
cd ifood-data-platform/tools-pyspark
grep --invert-match pyspark requirements.txt > requirements-dbconnect.txt
cd ../..

docker build -t databricks:5.5 \
    --build-arg MFA_DEVICE=$MFA_DEVICE \
    .
