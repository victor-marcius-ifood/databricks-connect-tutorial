#!/bin/bash

docker run -it --rm \
    --mount source=databricks-home,destination=/root \
    --mount type=bind,source=$(pwd),destination=/opt/pwd \
    databricks:5.5 \
    "$@"
