#!/bin/bash

MY_PROFILE="ifood-users"


store_session_token()
{
    TTOKEN=$1
    RESPONSE=($(aws sts get-session-token --serial-number "${MFA_DEVICE}" \
        --token-code ${TTOKEN} --duration-seconds 86400 --output text))
    if [ ${?} -ne 0 ]
    then
        echo "Error getting the session token. Continuing without session."
    else
        FILE_BEGIN_TOKEN="# BEGIN ${MY_PROFILE} temporary credentials"
        FILE_END_TOKEN="# END ${MY_PROFILE} temporary credentials"
        # remove already existing credentials if needed
        sed -i -r "/${FILE_BEGIN_TOKEN}/,/${FILE_END_TOKEN}/d" ${HOME}/.aws/config

        # write new credentials to file
        cat <<EOF >> ${HOME}/.aws/config
${FILE_BEGIN_TOKEN}
[profile ${MY_PROFILE}]
region = us-east-1
aws_access_key_id = ${RESPONSE[1]}
aws_secret_access_key = ${RESPONSE[3]}
aws_session_token = ${RESPONSE[4]}
[profile data-access-da-ds]
region = us-east-1
role_arn = arn:aws:iam::235655843873:role/Data_Access_DA_DS
source_profile = ${MY_PROFILE}
${FILE_END_TOKEN}
EOF

        UNTIL=${RESPONSE[2]}
        echo "Access granted until ${UNTIL}"
        echo "* Saved to file ${HOME}/.aws/config as ${MY_PROFILE} profile."
    fi
}


if [ -z "$MFA_DEVICE" ]
then
    echo "Environment variable MFA_DEVICE is empty."
elif [ ! -e "$HOME/.aws/credentials" ]
then
    echo "WARN: Missing AWS credentials. Run aws configure."
else

    read -p "Enter the token code for ${MFA_DEVICE} [Leave empty to skip a new session setup]: " TTOKEN
    if [ -n "$TTOKEN" ]
    then
        store_session_token "$TTOKEN"
    fi
    export AWS_PROFILE=data-access-da-ds
fi

if [ ! -e "$HOME/.databricks-connect" ]
then
    echo "WARN: Missing databricks connect configuration. Run databricks-connect configure"
fi

source /opt/env-databricks/bin/activate
exec "$@"
